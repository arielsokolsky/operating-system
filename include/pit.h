#ifndef PIC_h
#define PIC_h

#include "screen.h"
#include "system.h"

void timerHandler();

void init_timer(uint32 frequency);

#endif