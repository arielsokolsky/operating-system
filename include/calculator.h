#ifndef CALCULATOR 
#define CALCULATOR
#include "screen.h"

int fibonacci(int num);
int* sort(int* array, int len);
int sum(int* array, int len);
int sub(int* array, int len);


#endif
