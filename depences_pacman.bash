
#install nasm
sudo pacman -S nasm

#install gcc
sudo pacman -S build-essential
sudo pacman -S manpages-dev
sudo pacman -S gcc

#install qemu
sudo pacman -S qemu

#install xorriso
sudo pacman -S xorriso