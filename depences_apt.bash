sudo apt update

#install nasm
sudo apt-get install nasm

#install gcc
sudo apt install build-essential
sudo apt-get install manpages-dev
sudo apt install gcc

#install qemu
sudo apt install qemu

#install xorriso
sudo apt-get install xorriso